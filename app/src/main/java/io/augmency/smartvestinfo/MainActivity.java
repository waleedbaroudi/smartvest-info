package io.augmency.smartvestinfo;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    Spinner frequencySpinner;
    TableRow row1, row2, row3, row4;
    TextView data1, data2, data3, data4;
    Button noDataButton, partialDataButton, fullDataButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeUIComponents();

    }

    private void initializeUIComponents() {
        // initialize frequency select
        frequencySpinner = findViewById(R.id.frequency_spinner);
        ArrayAdapter<String> frequencyAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.frequency_array));
        frequencyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        frequencySpinner.setAdapter(frequencyAdapter);
        frequencySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String toastText = "Data will update every " + frequencySpinner.getSelectedItem();
                Toast.makeText(MainActivity.this, toastText, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        // initialize data rows
        row1 = findViewById(R.id.row_1);
        row2 = findViewById(R.id.row_2);
        row3 = findViewById(R.id.row_3);
        row4 = findViewById(R.id.row_4);

        data1 = findViewById(R.id.row_1_data);
        data2 = findViewById(R.id.row_2_data);
        data3 = findViewById(R.id.row_3_data);
        data4 = findViewById(R.id.row_4_data);

        // initialize control buttons
        noDataButton = findViewById(R.id.no_data_button);
        noDataButton.setOnClickListener(view -> hideData());

        partialDataButton = findViewById(R.id.partial_data_button);
        partialDataButton.setOnClickListener(view -> showPartialData());

        fullDataButton = findViewById(R.id.full_data_button);
        fullDataButton.setOnClickListener(view -> showFullData());
    }

    private void hideData() {
        row1.setVisibility(View.GONE);
        row2.setVisibility(View.GONE);
        row3.setVisibility(View.GONE);
        row4.setVisibility(View.GONE);
    }

    private void showPartialData() {
        row1.setVisibility(View.VISIBLE);
        row2.setVisibility(View.VISIBLE);
        row3.setVisibility(View.GONE);
        row4.setVisibility(View.GONE);
    }

    private void showFullData() {
        row1.setVisibility(View.VISIBLE);
        row2.setVisibility(View.VISIBLE);
        row3.setVisibility(View.VISIBLE);
        row4.setVisibility(View.VISIBLE);
    }
}